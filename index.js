var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');
var port = 8000;

app.get('/', (request, response) => {
	console.log('<Server> Receive Packet');
	response.sendFile(__dirname + '/views/index.html');
});

var onlineClientNumber = 0;

io.on('connection', (socket) => {
	
	console.log('<Socket> connection');
	onlineClientNumber++;
	socket.broadcast.emit('onlineClientNumber', onlineClientNumber);
	socket.emit('onlineClientNumber', onlineClientNumber);
	
	socket.on('disconnect', () => {
		onlineClientNumber--;
		console.log('<Socket> disconnect');
		socket.broadcast.emit('onlineClientNumber', onlineClientNumber);
	})

	socket.on('ServerGreet', () => {
		console.log('<Server> Receive Greet');
		socket.emit("ClientGreet", "Hi! Client");
	});
})

server.listen(port, () => {
	console.log('<Server> Server start');
});
